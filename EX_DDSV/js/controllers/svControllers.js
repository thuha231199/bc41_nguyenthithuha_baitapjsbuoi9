
function layThongTinTuForm() {
    //lấy thông tin từ form
    var _maSV = document.getElementById("txtMaSV").value;
    var _tenSV = document.getElementById("txtTenSV").value;
    var _emailSV = document.getElementById("txtEmail").value;
    var _matKhauSV = document.getElementById("txtPass").value;
    var _diemToan = document.getElementById("txtDiemToan").value;
    var _diemLy = document.getElementById("txtDiemLy").value;
    var _diemHoa = document.getElementById("txtDiemHoa").value;
    // tạo object sv
    return new SinhVien(
        _maSV,
        _tenSV,
    _emailSV,
    _matKhauSV,
    _diemToan,
    _diemHoa,
    _diemLy,
        )
}
function renderDSSV(svArr) {
    //render danh sách sinh viên 
    //contentHTML ~ chuỗi chứa thẻ tr
    // 1 thẻ <tr></tr> chứa nhiều thẻ <td></td>
    var contentHTML = "";
    for (var index=0; index < svArr.length; index++) {
        var sv = svArr[index];
        //DDSV tại vị trí index
        var contentTr= `<tr>
                            <td>${sv.maSV}</td>
                            <td>${sv.tenSV}</td>
                            <td>${sv.emailSV}</td>
                            <td>${sv.tinhDTB()}</td>
                            <td>0</td>
                            <td>
                            <button
                            onclick ="xoaSV('${sv.maSV}')" 
                            class="btn btn-danger">Xóa</button>
                            <button 
                            onclick ="suaSV('${sv.maSV}')" 
                            class="btn btn-warning">Sửa</button>
                            </td>
                        </tr>`;
                        //ép thành chuỗi ''. vd: "xoaSV('${sv.maSV}')" 
        contentHTML=contentHTML + contentTr;
    }
    //show ra table
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
    //thẻ tr phải innerHTML mới show ra đc 
}


function timKiemViTri(id, arr) {
    var viTri = -1;
    for (var index = 0; index < arr.length; index++){
        if (arr[index].maSV == id){
            viTri = index;
        }
    }
    return viTri;
}