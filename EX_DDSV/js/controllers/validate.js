// hợp lệ  => return true


function kiemTraTrung(idSV,svArr){

    // var viTri = timKiemViaTri();
    //findIndex trả về vị trí của item nếu điều kiện true, nếu không tìm thấy sẽ return -1
    var viTri = svArr.findIndex(function (item){
        //indexOf-> tìm string hoặc number/  findIndex để tìm object
        return item.maSV == idSV;
    });
    if (viTri!= -1){
        document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại";
        return false;
    } else {
        document.getElementById("spanMaSV").innerText= "";
        return true;
    }

}


// muốn kiểm tra giá trị nào truyền value của giá trị đó vào + id của thẻ span ở dưới đó
function kiemTraDoDai(value,idErr, min, max) {
    var length = value.length;

    if (length<min || length>max) {
        document.getElementById(idErr).innerText=`Độ dài phải từ ${min} đến ${max} kí tự`;
        return false;
        //trước khi báo lỗi phải show ra lỗi
    } else {
        document.getElementById(idErr).innerText ="";
        return true;
        //muốn return true thì phải clear cho nó thành chuỗi rỗng
    }
}


function kiemTraEmail(value){
    const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if(isEmail){
        document.getElementById("spanEmailSV").innerText= "";
        return true;
    } else {
        document.getElementById("spanEmailSV").innerText= "Email không đúng định dạng";
        return false;
    }
}