var DSSV = []

//B1: lấy dữ liệu từ localStorage khi user load lại trang (let lấy lên, get lưu xuống)
//dưới localStorage k có dữ liệu để lấy lên sẽ là null
let dataJson= localStorage.getItem("DSSV_LOCAL");
//setItem lưu xuống ; getItem lấy lên (lưu xuống tên gì lấy lên tên đó)
console.log("🚀 ~ file: main.js:5 ~ dataJson", dataJson)

//kiểm tra nếu có dât ở localStorage thì  mới gán vào aray DSSV
if (dataJson != null){
    var dataArr = JSON.parse(dataJson);
    //từ convert ngược lại thành array rồi lưu lại
    //dữ liệu lấy lên từ cocalStorage sẽ bị mất method => từ array không có method tinhDTB ==> convert thành array có method tinhDTB bằng map()
    DSSV = dataArr.map(function(item){
        var sv = new SinhVien(
            //new: lớp đôi tượng sinhvien sẽ tự tạo object gồn dữ liệu này có thêm tính đtb
            item.maSV,
            item.tenSV,
            item.emailSV,
            item.matKhauSV,
            item.diemToan,
            item.diemLy,
            item.diemHoa
            );
        return sv;
    })
}


//thêm SV
function themSV() {
var sv = layThongTinTuForm()



//validate ~ kiểm tra dữ liệu
var isValid = true; 
isValid = kiemTraTrung(sv.maSV, DSSV) &&
kiemTraDoDai(sv.maSV, "spanMaSV" , 6 ,8) 
isValid = isValid & kiemTraEmail(sv.EmailSV)
if (isValid){
    
    
    //push sv vào DSSV
    DSSV.push(sv);
    
    
    //convert array DSSV thành json
    var dssvJson = JSON.stringify(DSSV);
    //json là object (đối tượng)
    //json hay cocalStorage đều là có sẵn
    //json k lưu đc function, nếu là function thì sẽ đc remove
    
    
    //lưu json vào localStorage (localStorage chỉ lưu đc json)
    localStorage.setItem("DSSV_LOCAL",dssvJson);
    //setItem là lưu xuống
    
    
    renderDSSV(DSSV);//(hàm chỉ đc chạy khi nhấn nút thêm)
}    
}


    
    // xóa sv(hiện tại sv nào có ãm là số không thì nó ok còn chữ "abc" thì không)
    function xoaSV(idSV){
        //splice(vị trí, số lượng)
        /*for (var index=0; index<DSSV.length; index++){
            if(DSSV[index].maSV == idSV){
                // . tối lớp đối tượng trong 1 
                viTri=index;
            }
            //SV nào mã là số k thì xóa đc
        }
         * 
         */
        var viTri = timKiemViTri(idSV, DSSV);
    if (viTri != -1) {
        DSSV.splice(viTri, 1);
        renderDSSV(DSSV)
    }
}


//sửa sv (k cho sửa mã SV -> search disabled input js)
function suaSV(idSV) {
    var viTri = timKiemViTri(idSV, DSSV);
    if (viTri != -1){
        var sv = DSSV[viTri];
        console.log("🚀 ~ file: main.js:46 ~ suaSV ~ sv", sv)
        document.getElementById("txtMaSV").disabled = true;
}
}



// var color = ["red","blue"];
// var newColor = color.map(function(item){
//     return "alice" +item;  -> alice red, alice blue
// })
// console.log("🚀 ~ file: main.js:62 ~ newColor ~ newColor ", newColor )