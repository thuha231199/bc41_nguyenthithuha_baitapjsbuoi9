function hienThiThongTin() {
//lấy thông tin từ form
    var _maSv = document.getElementById("txtMaSV").value;
    var _tenSv = document.getElementById("txtTenSV").value;
    var _loai = document.getElementById("loaiSV").value;
    var _diemToan = document.getElementById("txtDiemToan").value*1;
    var _diemVan = document.getElementById("txtDiemVan").value*1;
    console.log({
        _maSv,
        _tenSv,
        _loai,
        _diemToan,
        _diemVan,
    });


//tạo object sv: thuộc tính là những key chứa dữ liệu (data: string, number)/ phương thức là những key chưa function
    var sv = {
        maSv: _maSv,
        tenSv: _tenSv,
        loaiSv: _loai,
        diemToan: _diemToan,
        diemVan: _diemVan,
// method nó cũng là 1 function nên đặt phải có động từ (vd: tinhDTB)
        tinhDTB:function() {
            return (this.diemToan + this.diemVan)/2;
        },
        xepLoai(){
            var diem = this.tinhDTB();
            if (diem>=5){
                return "Đạt";
            } else {
                return "Rớt";
            }
        }
    };
    //show thông tin 
    document.getElementById("spanTenSV").innerText=sv.tenSv;
    document.getElementById("spanMaSV").innerText=sv.maSv;
    document.getElementById("spanLoaiSV").innerText=sv.loaiSv;
    document.getElementById("spanDTB").innerText=sv.tinhDTB();
    //do tinhDTB là function nên phải sv.tinhDTB()
    document.getElementById("spanXepLoai").innerText=sv.xepLoai();
}









