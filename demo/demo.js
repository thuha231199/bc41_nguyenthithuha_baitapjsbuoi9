var cat1 = {
    name: "mun",
    age: 2,
    //name: "mun",
    //age: 2,  -> là phương thức
    speak: function(){
        console.log("meo meo", this.name);
        //this: gọi tới cùng cấp 
        //(tùy nào nơi mình code sẽ có ngữ cảnh khác nhau, this đại diện cho cái nào thì gọi là ngữ cảnh)
    },
    // children:[
    //     {
    //         name:"min",
    //         age:0.2 
    //     },
    // ],
};
/* 
var variable = {
    key : value
}
*/


//array => index ~ có thứ tự (index : 0, 1, 2,...)
//object => key ~ không có thứ tự (chỉ có key)
//truy xuất value qua key
var catName = cat1.name;


// console.log(cat1.name)


//update value cho key

cat1.name = "bull";
console.log("🚀 ~ file: demo.js:24 ~ cat1", cat1)
cat1.speak()
//gọi hàm luôn có dấu ngoặc tròn (): cat1.speak()



//var catAge = cat1.age;
var catAge = cat1["age"];

var key ="age"; //var key ="name";
var value =3 ;  //var value = "minmax" ;
cat1[key] = value;
//update theo ý muốn k cần phải thay đổi (cố định)


cat1[key] = value;
console.log("🚀 ~ file: demo.js:46 ~ cat1", cat1)



//pass by value: string, number, boolean
//pass by reference: array object 

var a =2 ;
var b = a;
b = 5 ; 
console.log("🚀 ~ file: demo.js:59 ~ a", a)


var cat2 = cat1;
cat2.name = "Mực"
var months = ['Jan', 'March', 'April', 'May'];
months.slice(1, 1,"Feb")


var dog1={
    name:"Quách đen",
    age: 2,
};

var dog2 = {
    ten: "Quách mực",
    tuoi: 2,
};

//tên lớp đối tượng phải viết hoa chữ cái đầu tiên
function Dog(_name,_age){
this.name = _name; 
this.age = _age;
this.speak = function() {
    //con chó có phương thứ là nói chuyện thì chỉ cần truyền dữ liệu vô
    console.log("Gâu Gâu tao là:", this.name)
}
}


var dog3= new Dog("Quách xanh", 3);
var dog4= new Dog("Quách vàng", 3)
dog3.speak()
console.log("🚀 ~ file: demo.js:90 ~ dog3", dog3)
console.log("🚀 ~ file: demo.js:92 ~ dog4", dog4)
